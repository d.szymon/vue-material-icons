﻿// See https://aka.ms/new-console-template for more information

using System;
using System.IO;
using System.Linq;
using vue_material_icons.Services;

const string resultsDir = "./Results";
var metaService = new IconsMetadataService();
var resolver = new IconsResolver();
var converter = new IconsConverter();

var data = await metaService.GetIcons();

Console.WriteLine("Pobrano metadane dla {0} ikon", data.Icons.Count);

foreach (var cat in IconsResolver.CategoriesMap.Where(cat => !Directory.Exists(Path.Join(resultsDir, cat.Key))))
{
    Directory.CreateDirectory(Path.Join(resultsDir, cat.Key));
}

var i = 0;
foreach (var icon in data.Icons)
{
    var sources = await resolver.FetchSvg(icon);

    foreach (var source in sources)
    {
        var res = converter.BuildVueComponent(icon, source.Key, source.Value);
        File.WriteAllText(Path.Join(resultsDir, source.Key, $"Icon{icon.FormattedName}.vue"), res);
    }
    
    Console.WriteLine("Załadowano {0} ({1}/{2})", icon.Name, ++i, data.Icons.Count);
}