﻿using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace vue_material_icons.Services
{
    public class IconsConverter
    {
        protected readonly string template;
        protected const string ExcludedPath = "M0 0h24v24H0V0z";

        public IconsConverter()
        {
            this.template = File.ReadAllText("./Templates/icon.template.vue");
        }

        public string BuildVueComponent(IconsResponse.SingleIcon icon, string category, string svg)
        {
            var result = this.template
                .Replace("{{name}}", icon.FormattedName);
            var doc = XDocument.Parse(svg);

            result = result.Replace("{{viewbox}}", doc.Root.Attribute("viewBox").Value);
            return result.Replace("{{content}}", BuildRenderMethod(doc.Root));
        }

        protected string BuildRenderMethod(XElement element)
        {
            if (ExcludedPath.Equals(element.Attribute("d")?.Value))
            {
                return string.Empty;
            }
            
            var attrs = string.Empty;
            var children = string.Empty;
            var isRoot = element.Attribute("xmlns") != null;

            foreach (var attribute in element.Attributes())
            {
                if (attrs.Length > 0)
                {
                    attrs += "\n";
                }

                if (isRoot && (attribute.Name.LocalName.Equals("width") || attribute.Name.LocalName.Equals("height")))
                {
                    attrs += $"'{attribute.Name.LocalName}': this.size,";
                }
                else
                {
                    attrs += $"'{attribute.Name.LocalName}': '{attribute.Value}',";
                }
            }

            if (attrs.Length > 0)
            {
                attrs = "\n{\n" + attrs + "\n},";
            }

            if (element.Descendants().Count() > 0)
            {
                foreach (var node in element.Descendants())
                {
                    var method = BuildRenderMethod(node);
                    
                    if (string.IsNullOrEmpty(method))
                    {
                        continue;
                    }
                    
                    children += method + ",\n";
                }

                children = "[\n" + children + "],\n";
            }
            
            return $"h(\n'{element.Name.LocalName}',{attrs}\n{children})";
        }
    }
}