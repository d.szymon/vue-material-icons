﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace vue_material_icons.Services
{
    public class IconsMetadataService
    {
        protected const string IconsEndpoint = "https://fonts.google.com/metadata/icons";
        
        public async Task<IconsResponse> GetIcons()
        {
            var client = new HttpClient();
            var result = await client.GetStringAsync(IconsEndpoint);

            if (result == null)
            {
                throw new Exception("Get icons = null");
            }

            var icons = JsonSerializer.Deserialize<IconsResponse>(result.Substring(4),
                new JsonSerializerOptions()
                {
                    PropertyNameCaseInsensitive = true,
                });

            if (icons == null)
            {
                throw new Exception("icons == null");
            }

            return icons;
        }
    }

    public class IconsResponse
    {
        [JsonPropertyName("asset_url_pattern")]
        public string AssetUrlPattern { get; set; }
        public IReadOnlyList<string> Families { get; set; }
        public string Host { get; set; }
        public IReadOnlyList<SingleIcon> Icons { get; set; }

        public class SingleIcon
        {
            public IReadOnlyList<string> Categories { get; set; }
            public long Codepoint { get; set; }
            public string Name { get; set; }
            public long Popularity { get; set; }
            [JsonPropertyName("sizes_px")]
            public IReadOnlyList<ushort> Sizes { get; set; }
            public IReadOnlyList<string> Tags { get; set; }
            [JsonPropertyName("unsupported_families")]
            public IReadOnlyList<string> UnsupportedFamilies { get; set; }
            public ushort Version { get; set; }

            public string FormattedName => string.Join(string.Empty,
                Name.Split('_').Select(s => string.Concat(s[..1].ToUpper(), s.AsSpan(1))));
        }
    }
}