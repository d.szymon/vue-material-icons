﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace vue_material_icons.Services
{
    public class IconsResolver
    {
        protected const string Url = "https://fonts.gstatic.com/s/i/{0}/{1}/v{2}/24px.svg";

        public static readonly Dictionary<string, string> CategoriesMap;

        static IconsResolver()
        {
            CategoriesMap = new Dictionary<string, string>()
            {
                {"Outlined", "materialiconsoutlined"},
                {"Filled", "materialicons"},
                {"Round", "materialiconsround"},
                {"Sharp", "materialiconssharp"},
                {"TwoTone", "materialiconstwotone"}
            };
        }

        public async Task<string?> FetchSvg(IconsResponse.SingleIcon icon, string type)
        {
            var client = new HttpClient();
            return await client.GetStringAsync(string.Format(Url, type, icon.Name, icon.Version));
        }

        public async Task<Dictionary<string, string>> FetchSvg(IconsResponse.SingleIcon icon)
        {
            var tasks = CategoriesMap
                .Select(item => FetchSvg(icon, item.Value))
                .ToImmutableArray();

            await Task.WhenAll(tasks);

            var results = new Dictionary<string, string>(CategoriesMap.Count);

            for (var i = 0; i < tasks.Length; i++)
            {
                var task = tasks.ElementAt(i);
                var content = await task;

                if (content == null)
                {
                    Console.WriteLine("Pominięto rodzaj {0} dla {1}", CategoriesMap.Values.ElementAt(i), icon.Name);
                    continue;
                }
                
                results.Add(CategoriesMap.Keys.ElementAt(i), content);
            }

            return results;
        }
    }
}